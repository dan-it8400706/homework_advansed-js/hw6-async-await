const btn = document.querySelector('.btn')
btn.addEventListener("click", () => {
    showData()
})
const p = document.querySelector('.text')
console.log(p)

async function showData() {

    // запит JSON в ір користувача
    let response = await fetch('https://api.ipify.org/?format=json');
    let ip = await response.json();
  
    // запит данних по ір : континент, країна, регіон, місто, район(no district).
    let data = await fetch(`http://ip-api.com/json/${ip.ip}?fields=continent,country,region,regionName,city,district`);
    let resalt = await data.json();
    console.log(resalt)
    console.log()
    p.insertAdjacentHTML("beforeend", `
    ${resalt.continent} <br>
    ${resalt.country} <br>
    ${resalt.regionName} <br>
    ${resalt.city}`) 
}